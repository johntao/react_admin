import './App.css';
import 'antd/dist/antd.css'; 
import { BrowserRouter as Router } from "react-router-dom";
import { main as mainConfig } from "./router/index";
import { RenderRoutes } from "./router/utils";



function App() {
  return (
    <Router>
      <div className="App">
        <RenderRoutes routes={mainConfig}></RenderRoutes>
      </div>
    </Router>
  );
}
// function App() {
//   return (
//     <div className="App">
//       <header className="App-header">
//         <img src={logo} className="App-logo" alt="logo" />
//         <p>
//           Edit <code>src/App.js</code> and save to reload.
//         </p>
//         <a
//           className="App-link"
//           href="https://reactjs.org"
//           target="_blank"
//           rel="noopener noreferrer"
//         >
//           Learn React
//         </a>
//       </header>
//     </div>
//   );
// }

export default App;



