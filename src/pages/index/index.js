import React, { Component } from "react";
import { Layout,  Menu } from "antd";
import { RenderRoutes } from "../../router/utils";
import { Link } from "react-router-dom";
// require("./index.less");

const { Header, Content, Sider } = Layout;
const { SubMenu } = Menu;

class Index extends Component {
  state = {
    collapsed: false,
    theme: "dark",
  };

  onCollapse = (collapsed) => {
    console.log(collapsed);
    this.setState({ collapsed });
  };
  changeTheme = (value) => {
    this.setState({
      theme: value ? "dark" : "light",
    });
  };
  render() {
    const { collapsed } = this.state;
    let { routes } = this.props;
    console.log(routes);
    return (
      <Layout style={{ minHeight: "100vh" }}>
        <Sider
          theme={this.state.theme}
          collapsible
          collapsed={collapsed}
          onCollapse={this.onCollapse}
        >
          <div className="logo" />
          <Menu
            theme={this.state.theme}
            defaultSelectedKeys={["/user/experience"]}
            defaultOpenKeys={["sub1"]}
            mode="inline"
          >
            <Menu.Item key="1">
              <Link to="/user/WorkBench">工作台</Link>
            </Menu.Item>
            <SubMenu key="sub1" title="用户管理">
              <Menu.Item key="/user/experience">
                <Link to="/user/experience">体验用户</Link>
              </Menu.Item>
              <Menu.Item key="4">VIP用户</Menu.Item>
              <Menu.Item key="5">
                <Link to="/user/Details">用户详情</Link>
              </Menu.Item>
            </SubMenu>
          </Menu>
        </Sider>
        <Layout className="site-layout">
          <Header className="ant-layout-header" style={{ background: "#fff" }}>
             1
          </Header>
          <Content style={{ margin: "16px 16px" }}>
            <RenderRoutes routes={routes}></RenderRoutes>
          </Content>
        </Layout>
      </Layout>
    );
  }
}
export default Index;

// import React, { Component } from "react";
// // require("./index.less");
// class Index extends Component {
//   render() {
//     return (
//       <div className="main-page">
//         首页
//       </div>
//     );
//   }
// }
// export default Index ;